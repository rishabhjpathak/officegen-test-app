import React from "react";
import moment from "moment";
import _ from "lodash";
import { saveAs } from "file-saver";
import creditNote from "./creditNote.json";
import salesInvoice from "./salesInvoice.json";
import deliveryOrder from "./deliveryOrder.json";
import receipt from "./receipt.json";
import forecastData from "./forecastData.json";

import {
  getJsonForCreditNoteDocx,
  getJsonForPaymentReceipt,
  getJsonForSalesInvoice,
  getJsonForDeliveryOrderDocx,
} from "./helper";

export default function Officegen() {
  const onClick = async () => {
    try {
      console.log("Starting......");
      const endpoint = "docx";
      // const endpoint = "sheet";
      const doc = { filename: "delivery_order.docx", data: getJsonForDeliveryOrderDocx(deliveryOrder) };
      // const doc = { filename: "credit_note.docx", data: getJsonForCreditNoteDocx(creditNote) };
      // const doc = { filename: "sales_invoice.docx", data: getJsonForSalesInvoice(salesInvoice) };
      // const doc = { filename: "payment_receipt.docx", data: getJsonForPaymentReceipt(receipt) };
      // const doc = { filename: "forecast.xlsx", data: generateData(forecastData) };
      // const endpoint = "credit_note";
      // const endpoint = "sales_invoice";
      // const url = `https://logic.dev.fefifo.co:443/v1.0/api/officegen/${endpoint}`;
      // const url = `http://localhost:5000/${endpoint}`;
      const url = `http://localhost:3002/v1.0/api/officegen/${endpoint}`;
      const res = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(doc.data),
      });
      console.log("Response: ", res);
      const data = await res.blob();
      console.log("Finished......", data, res);
      return saveAs(data, doc.filename);
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div>
      <button onClick={onClick}>Save</button>
    </div>
  );
}

const generateData = (array) => {
  const sheetName = "Sheet name";
  const widthArray = [
    { colId: 0, width: 30 },
    { colId: 1, width: 25 },
    { colId: 2, width: 15 },
    { colId: 3, width: 15 },
    { colId: 4, width: 15 },
    { colId: 5, width: 15 },
    { colId: 6, width: 15 },
    { colId: 7, width: 15 },
  ];
  const headers = [
    "Farmspace",
    "Harvest Period",
    "Expected Start Date",
    "Expected Delivery Date",
    "Model Baseline",
    "Adjusted Baseline",
    "Confirmed (7 Day)",
    "Confirmed (3 Day)",
  ];
  const data = array.map((o) => {
    const adjustedBaseline = Number(Number(o.amt) + Number(o.amt) * (Number(o.adjustPct) / 100)).toFixed(1);

    return [
      `${o.agroProfile.name.fullName}\r\n${o.agroProfile.code}\r\n${o.agroProfile.assignedCropPlotCode}\r\n${o.agroProfile.assignedCropModelCode}`,
      `Cycle: ${o.cycleNo}\r\nSection: ${o.section}\r\nHarvest: ${o.period}`,
      moment(o.start).format("DD/MM/YYYY"),
      moment(o.deliveryDate).format("DD/MM/YYYY"),
      o.amt,
      adjustedBaseline,
      o.day7Status === "PENDING" ? "-" : _.capitalize(o.day7Status),
      o.day3Status === "PENDING" ? "-" : _.capitalize(o.day3Status),
    ];
  });
  return [{ data, headers, sheetName, widthArray }];
};
