import moment from "moment";

const darkOpts = {
  b: true,
  sz: "18",
  color: "ffffff",
  shd: {
    fill: "000000",
    themeFill: "text1",
    themeFillTint: "99",
  },
  fontFamily: "Calibri",
};

const tableHeading = [
  {
    val: "S/N",
    opts: { ...darkOpts, cellColWidth: 2000 },
  },
  {
    val: "Description",
    opts: { ...darkOpts, cellColWidth: 20000 },
  },
  {
    val: "Qty",
    opts: { ...darkOpts, cellColWidth: 4000, align: "center" },
  },
  {
    val: "UOM",
    opts: { ...darkOpts, cellColWidth: 4000, align: "center" },
  },
  {
    val: "U/Price (MYR)",
    opts: { ...darkOpts, cellColWidth: 4000, align: "center" },
  },
  {
    val: "Amount (MYR)",
    opts: { ...darkOpts, cellColWidth: 4000, align: "center" },
  },
];

const tableRow = (index, desc, qty, unit, price, amount) => {
  const opts = {
    sz: "16",
    align: "center",
    vAlign: "center",
  };
  return [
    {
      val: index,
      opts: { ...opts },
    },
    {
      val: desc,
      opts: { ...opts, align: "left" },
    },
    {
      val: qty,
      opts: { ...opts },
    },
    {
      val: unit,
      opts: { ...opts },
    },
    {
      val: price,
      opts: { ...opts },
    },
    {
      val: amount,
      opts: { ...opts },
    },
  ];
};

const getThankYouTable = () => {
  const thankYouTable = [
    [{ val: "Thank you,", opts: { cellColWidth: 10000, sz: 18 } }],
    [
      { val: "FEFIFO MALAYSIA SDN BHD ", opts: { cellColWidth: 10000, sz: 18, b: true } },
      { val: "This is system generated. No signature required.", opts: { cellColWidth: 10000, sz: 18 } },
    ],
  ];
  const thankYouTableStyle = {
    borders: false,
    tableFontFamily: "Calibri",
    tableSize: 10,
    align: "left",
    tableAlign: "left",
  };

  return [{ type: "table", val: thankYouTable, opt: thankYouTableStyle }];
};

const paymentAndThankyouData = () => {
  const paymentTableStyle = {
    borders: false,
    tableFontFamily: "Calibri",
    tableSize: 10,
    align: "left",
    tableAlign: "left",
  };
  const paymentTable = [
    [
      {
        val: "Please make payment by bank transfer to the following:",
        opts: { sz: 18, bold: true, cellColWidth: 10000 },
      },
      {
        val: "",
        opts: { font_size: 14, bold: true, cellColWidth: 10000 },
      },
    ],
    [
      { val: "Bank Acct Name: Fefifo Malaysia Sdn Bhd", opts: { sz: 18 } },
      { val: "Bank Acct No.: 8010169358", opts: { sz: 18 } },
    ],
    [
      { val: "Bank Name: CIMB Bank Berhad", opts: { sz: 18 } },
      { val: "Swift Code: CIBBMYKL", opts: { sz: 18 } },
    ],
  ];

  const thankyou = getThankYouTable();

  return [
    { type: "text", val: "" },
    { type: "table", val: paymentTable, opt: paymentTableStyle },
    { type: "text", val: "" },
    // { type: "table", val: thankYouTable, opt: thankYouTableStyle },
    ...thankyou,
    { type: "pagebreak" },
  ];
};

export const getJsonForCreditNoteDocx = (creditNote) => {
  try {
    const getDetailsTable = (data) => [
      [
        {
          val: "Credit Note:.",
          opts: {
            cellColWidth: 5000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.creditNoteNo,
          opts: {
            cellColWidth: 10000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Issue Date:",
          opts: {
            cellColWidth: 5000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.issueDate,
          opts: {
            cellColWidth: 10000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Bill To:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: `${data.orderedBy.name}\r
      ${data.orderedBy.address}`,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Original Invoice No.:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.invoiceNo,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Attn:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.contact.name,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
            b: true,
          },
        },
        {
          val: "Original Invoice Date:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.invoiceDate,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Contact",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: `+${data.contact.mobile.fullNumber}`,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Buyer's Ref.:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: "",
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
    ];

    const detailsTable = getDetailsTable(creditNote);

    const harvestRow = (harvest) => {
      const opts = {
        sz: "16",
        vAlign: "center",
        align: "center",
      };
      return [
        {
          val: `${harvest.agroProfile.name.fullName}\r
      ${harvest.agroProfile.code}\r
      ${harvest.agroProfile.assignedCropPlotCode}\r
      ${harvest.agroProfile.assignedCropModelCode}
    `,
          opts: { ...opts, align: "left" },
        },
        {
          val: `Cycle: ${harvest.cycleNo}\r
    Section: ${harvest.sectionNo}\r
    Harvest: ${harvest.harvestPeriod}`,
          opts: { ...opts, align: "left" },
        },
        { val: `${harvest.gradeAYield} kg`, opts: { ...opts } },
        { val: `${harvest.nonGradeAYield} kg`, opts: { ...opts } },
        { val: `${harvest.gradeArejectAmt || 0} kg`, opts: { ...opts } },
        { val: `${harvest.nonGradeArejectAmt || 0} kg`, opts: { ...opts } },
      ];
    };

    const tableStyle = {
      align: "left",
      tableSize: 24,
      tableAlign: "left",
      borders: true,
      tableFontFamily: "Calibri",
    };

    let creditNoteTable = [tableHeading];

    for (let i = 0; i < creditNote.deliveryDetails.length; i++) {
      const cropModel = creditNote.deliveryDetails[i];
      const heading = [{ val: cropModel.cropModelCode, opts: { gridSpan: 6, sz: "16" } }];
      let gradeAContract = tableRow(
        1,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Grade A (Contracted)"}`,
        cropModel.creditNote.gradeAContractedQty,
        cropModel.unitValue,
        cropModel.gradeAPriceContract,
        cropModel.creditNote.gradeAContractedTotal !== 0
          ? `(${Math.abs(cropModel.creditNote.gradeAContractedTotalTotal)})`
          : 0
      );
      let gradeAUncontracted = tableRow(
        2,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Grade A (Uncontracted)"}`,
        cropModel.creditNote.gradeAUncontractedQty,
        cropModel.unitValue,
        cropModel.gradeAPriceSpot,
        cropModel.creditNote.gradeAUncontractedTotal !== 0
          ? `(${Math.abs(cropModel.creditNote.gradeAUncontractedTotal)})`
          : 0
      );
      let nonGradeA = tableRow(
        3,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Non Grade A"}`,
        cropModel.creditNote.nonGradeAQty,
        cropModel.unitValue,
        cropModel.nonGradeAPrice,
        cropModel.creditNote.nonGradeATotal !== 0 ? `(${Math.abs(cropModel.creditNote.nonGradeATotal)})` : 0
      );
      let appComm = tableRow(
        4,
        `${cropModel.cropLabel || "Applicable Commission"}`,
        cropModel.creditNote.appCommQty,
        cropModel.unitValue,
        cropModel.appComm,
        cropModel.creditNote.appCommTotal
      );

      creditNoteTable.push(heading);
      creditNoteTable.push(gradeAContract);
      creditNoteTable.push(gradeAUncontracted);
      creditNoteTable.push(nonGradeA);
      creditNoteTable.push(appComm);
    }

    creditNoteTable.push([
      { val: "Total", opts: { sz: "16", b: true, align: "right", gridSpan: 5 } },
      {
        val: creditNote.creditNoteTotal < 0 ? `(${Math.abs(creditNote.creditNoteTotal)})` : creditNote.creditNoteTotal,
        opts: { sz: "16", b: true, align: "center" },
      },
    ]);

    const terms = [
      "Please deduct this amount from the Original Invoice No. stated above.",
      "This is part of a consolidated invoice of accounts receivable due to Fefifo, the exclusively authorised agent for Agropreneurs (sellers).",
      "All payments collected are on behalf of respective Agropreneurs (sellers) for crops produced and received by the buyer.",
      "Breakdown of participating Agropreneurs (sellers) for this sales rejects breakdown enclosed hereafter.",
      "Payment made by bank transfer, please quote our original invoice no. and email the bank slip after payment.",
    ];
    const termsTable = [];
    for (let term of terms) {
      termsTable.push([{ val: term, opts: { sz: "16", i: true, align: "left", cellColWidth: 20000 } }]);
    }

    const style = {
      "@w:val": "single",
      "@w:sz": "3",
      "@w:space": "1",
      "@w:color": "000000",
    };
    const borderStyle = {
      "w:top": style,
      "w:bottom": style,
      "w:left": style,
      "w:right": style,
    };

    const extra = paymentAndThankyouData();

    const page2 = [
      {
        type: "text",
        val: "SAJES REJECTS BREAKDOWN",
        opt: { bold: true, font_size: 14 },
        lopt: { align: "center" },
      },
    ];
    // docx.createP({ align: "center" }).addText("SALES REJECTS BREAKDOWN", { bold: true, font_size: 14 });
    const salesRejectsTable = { borders: true, tableFontFamily: "Calibri" };
    const opts = {
      sz: "16",
    };
    const labeledHeading = [
      { val: "Assigned Rejects", opts: { ...opts, cellColWidth: 20000, b: true, align: "center" } },
      { val: "Harvest Details", opts: { ...opts, cellColWidth: 7000, b: true } },
      { val: "Grade A Yield", opts: { ...opts, cellColWidth: 7000, b: true } },
      { val: "Non-Grade A Yield", opts: { ...opts, cellColWidth: 7000, b: true } },
      { val: "Grade A Reject", opts: { ...opts, cellColWidth: 7000, b: true } },
      { val: "Non-Grade A Reject", opts: { ...opts, cellColWidth: 7000, b: true } },
    ];
    for (let i = 0; i < creditNote.deliveryDetails.length; i++) {
      const cropModel = creditNote.deliveryDetails[i];
      page2.push({ type: "text", val: "" });
      page2.push({ type: "text", val: cropModel.cropModelCode, opt: { bold: true, font_size: 12 } });
      let rejectsTable = [];
      for (let harvest of cropModel.harvestPeriods) {
        rejectsTable.push(labeledHeading);
        rejectsTable.push(harvestRow(harvest));
      }
      page2.push({ type: "table", val: rejectsTable, opt: { ...salesRejectsTable } });
    }

    const data = [
      {
        type: "text",
        val: "Credit Note",
        opt: { bold: true, font_size: 14 },
        lopt: { align: "center" },
      },
      { type: "table", val: detailsTable, opt: tableStyle },
      { type: "text", val: "" },
      { type: "table", val: creditNoteTable, opt: tableStyle },
      { type: "text", val: "" },
      { type: "text", val: "Notes/Terms:", opt: { font_size: 8 } },
      { type: "table", val: termsTable, opt: { ...tableStyle, borderStyle } },
      ...extra,
      ...page2,
    ];

    return data;
  } catch (error) {
    console.log("error in getJSONForCreditNote", error);
  }
};

export const getJsonForSalesInvoice = (salesInvoice) => {
  try {
    const salesBatchRow = (harvest) => {
      const style = {
        "@w:val": "single",
        "@w:sz": "3",
        "@w:space": "1",
        "@w:color": "000000",
      };
      const borderStyle = {
        "w:top": style,
        "w:bottom": style,
        "w:left": style,
        "w:right": style,
      };

      const opts = {
        sz: "18",
        vAlign: "center",
        align: "center",
        // borderStyle
      };
      return [
        {
          val: `${harvest.agroProfile.name.fullName}\r
          ${harvest.agroProfile.code}\r
          ${harvest.agroProfile.assignedCropPlotCode}\r
          ${harvest.agroProfile.assignedCropModelCode}
        `,
          opts,
        },
        {
          val: `Cycle: ${harvest.cycleNo}\r
        Section: ${harvest.sectionNo}`,
          opts,
        },
        { val: `${harvest.gradeAYield} kg`, opts },
        { val: `${harvest.nonGradeAYield} kg`, opts },
      ];
    };
    const packagingRow = (harvest) => {
      const gradeA = Number(harvest?.gradeAContainer) > 0 ? harvest?.gradeAContainer : harvest?.gradeASalesPacks;
      const gradeALabel = Number(harvest?.gradeAContainer) > 0 ? "shipping containers" : "sales packs";
      const nonGradeA =
        Number(harvest?.nonGradeAContainer) > 0 ? harvest?.nonGradeAContainer : harvest?.nonGradeASalesPacks;
      const nonGradeALabel = Number(harvest?.nonGradeAContainer) > 0 ? "shipping containers" : "sales packs";
      const opts = {
        sz: "18",
        vAlign: "center",
        align: "center",
      };
      return [
        "",
        "",
        {
          val: `\r
          ${gradeA}\r
        ${gradeALabel}\r
        \r`,
          opts,
        },
        {
          val: `\r
          ${nonGradeA}\r
        ${nonGradeALabel}\r
        \r`,
          opts,
        },
      ];
    };

    const detailsTable = [
      [
        {
          val: "Invoice No:.",
          opts: {
            cellColWidth: 5000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: salesInvoice.invoiceNo,
          opts: {
            cellColWidth: 10000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Issue Date:",
          opts: {
            cellColWidth: 5000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: salesInvoice.issueDate,
          opts: {
            cellColWidth: 10000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Bill To:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: `${salesInvoice.orderedBy.name}\r
          ${salesInvoice.orderedBy.address}`,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Due Date:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: salesInvoice.dueDate,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Attn:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: salesInvoice.contact.name,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
            b: true,
          },
        },
        {
          val: "Terms:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: salesInvoice.terms,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Contact",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: `+${salesInvoice.contact.mobile.fullNumber}`,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Buyer's Ref.:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: "",
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "",
          opts: {
            cellColWidth: 5000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: "",
          opts: {
            cellColWidth: 10000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "DO No:.",
          opts: {
            cellColWidth: 5000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: salesInvoice.deliveryOrderNo,
          opts: {
            cellColWidth: 10000,
            align: "left",
            sz: "16",
          },
        },
      ],
    ];

    const tableStyle = {
      align: "left",
      tableSize: 24,
      tableAlign: "left",
      borders: true,
      tableFontFamily: "Calibri",
    };

    let salesInvoiceTable = [tableHeading];

    for (let i = 0; i < salesInvoice.deliveryDetails.length; i++) {
      const cropModel = salesInvoice.deliveryDetails[i];
      const heading = [{ val: cropModel.cropModelCode, opts: { gridSpan: 6, sz: "16" } }];
      let gradeAContract = tableRow(
        1,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Grade A (Contracted)"}`,
        cropModel.salesDetails.gradeAContractedQty,
        cropModel.unitValue,
        cropModel.gradeAPriceContract,
        cropModel.salesDetails.gradeAContractedTotal !== 0
          ? `(${Math.abs(cropModel.salesDetails.gradeAContractedTotalTotal)})`
          : 0
      );
      let gradeAUncontracted = tableRow(
        2,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Grade A (Uncontracted)"}`,
        cropModel.salesDetails.gradeAUncontractedQty,
        cropModel.unitValue,
        cropModel.gradeAPriceSpot,
        cropModel.salesDetails.gradeAUncontractedTotal !== 0
          ? `(${Math.abs(cropModel.salesDetails.gradeAUncontractedTotal)})`
          : 0
      );
      let nonGradeA = tableRow(
        3,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Non Grade A"}`,
        cropModel.salesDetails.nonGradeAQty,
        cropModel.unitValue,
        cropModel.nonGradeAPrice,
        cropModel.salesDetails.nonGradeATotal !== 0 ? `(${Math.abs(cropModel.salesDetails.nonGradeATotal)})` : 0
      );
      let appComm = tableRow(
        4,
        `${cropModel.cropLabel || "Applicable Commission"}`,
        cropModel.salesDetails.appCommQty,
        cropModel.unitValue,
        cropModel.appComm,
        cropModel.salesDetails.appCommTotal
      );

      salesInvoiceTable.push(heading);
      salesInvoiceTable.push(gradeAContract);
      salesInvoiceTable.push(gradeAUncontracted);
      salesInvoiceTable.push(nonGradeA);
      salesInvoiceTable.push(appComm);
    }

    salesInvoiceTable.push([
      { val: "Total", opts: { sz: "16", b: true, align: "right", gridSpan: 5 } },
      {
        val: salesInvoice.total < 0 ? `(${Math.abs(salesInvoice.total)})` : salesInvoice.total,
        opts: { sz: "16", b: true, align: "center" },
      },
    ]);

    const terms = [
      "This is a consolidated invoice of accounts receivable due to Fefifo, the exclusively authorised agent for Agropreneurs (sellers).",
      "All payments collected are on behalf of respective Agropreneurs (sellers) for crops produced and received by the buyer.",
      "Breakdown of participating Agropreneurs (sellers) for this sales batch enclosed hereafter.",
      " Late interest fees of 1.5% per month will be charged on payment amount due.",
      "Payment to be made by bank transfer, please quote our invoice no. and email the bank slip after payment.",
    ];
    const termsTable = [];
    for (let term of terms) {
      termsTable.push([{ val: term, opts: { sz: "16", i: true, align: "left", cellColWidth: 20000 } }]);
    }

    const style = {
      "@w:val": "single",
      "@w:sz": "3",
      "@w:space": "1",
      "@w:color": "000000",
    };
    const borderStyle = {
      "w:top": style,
      "w:bottom": style,
      "w:left": style,
      "w:right": style,
    };

    const extra = paymentAndThankyouData();

    const page2 = [
      {
        type: "text",
        val: "SALES BATCH BREAKDOWN",
        opt: { bold: true, font_size: 14 },
        lopt: { align: "center" },
      },
    ];

    const salesBatchTableStyle = { tableFontFamily: "Calibri", borders: true };
    const opts = {
      sz: "16",
    };
    const labeledHeading = [
      { val: "", opts: { ...opts, cellColWidth: 15000 } },
      { val: "Harvest Details", opts: { ...opts, cellColWidth: 7000, b: true } },
      { val: "Grade A Yield", opts: { ...opts, cellColWidth: 7000, b: true } },
      { val: "Non-Grade A Yield", opts: { ...opts, cellColWidth: 7000, b: true } },
    ];
    for (let i = 0; i < salesInvoice.deliveryDetails.length; i++) {
      const cropModel = salesInvoice.deliveryDetails[i];
      page2.push({ type: "text", val: "" });
      page2.push({ type: "text", val: cropModel.cropModelCode, opt: { bold: true, font_size: 12 } });
      let salesBatchTable = [];
      for (let harvest of cropModel.harvestPeriods) {
        salesBatchTable.push(labeledHeading);
        salesBatchTable.push(salesBatchRow(harvest));
        salesBatchTable.push(packagingRow(harvest));
      }
      page2.push({ type: "table", val: salesBatchTable, opt: salesBatchTableStyle });
    }

    const data = [
      {
        type: "text",
        val: "Sales Invoice",
        opt: { bold: true, font_size: 14 },
        lopt: { align: "center" },
      },
      { type: "table", val: detailsTable, opt: tableStyle },
      { type: "text", val: "" },
      { type: "table", val: salesInvoiceTable, opt: tableStyle },
      { type: "text", val: "" },
      { type: "text", val: "Notes/Terms:", opt: { font_size: 8 } },
      { type: "table", val: termsTable, opt: { ...tableStyle, borderStyle } },
      ...extra,
      ...page2,
    ];

    return data;
  } catch (error) {
    console.log("error in getJSONForCreditNote", error);
  }
};

export const getJsonForDeliveryOrderDocx = (deliveryOrder) => {
  const getDetailsTable = (data) => [
    [
      {
        val: "DO NO.:",
        opts: {
          cellColWidth: 5000,
          align: "left",
          b: true,
          sz: "16",
        },
      },
      {
        val: data.deliveryOrderNo,
        opts: {
          cellColWidth: 10000,
          align: "left",
          sz: "16",
        },
      },
      {
        val: "Issue Date:",
        opts: {
          cellColWidth: 5000,
          align: "left",
          b: true,
          sz: "16",
        },
      },
      {
        val: data.issueDate,
        opts: {
          cellColWidth: 10000,
          align: "left",
          sz: "16",
        },
      },
    ],
    [
      {
        val: "Ordered By:",
        opts: {
          cellColWidth: 7000,
          align: "left",
          b: true,
          sz: "16",
        },
      },
      {
        val: `${data.orderedBy.name}\r
      ${data.orderedBy.address}`,
        opts: {
          cellColWidth: 7000,
          align: "left",
          sz: "16",
        },
      },
      {
        val: "Invoice No.:",
        opts: {
          cellColWidth: 7000,
          align: "left",
          b: true,
          sz: "16",
        },
      },
      {
        val: data.invoiceNo,
        opts: {
          cellColWidth: 7000,
          align: "left",
          sz: "16",
        },
      },
    ],
    [
      {
        val: "Name:",
        opts: {
          cellColWidth: 7000,
          align: "left",
          b: true,
          sz: "16",
        },
      },
      {
        val: data.contact.name,
        opts: {
          cellColWidth: 7000,
          align: "left",
          sz: "16",
          b: true,
        },
      },
      {
        val: "Delivery Date:",
        opts: {
          cellColWidth: 7000,
          align: "left",
          b: true,
          sz: "16",
        },
      },
      {
        val: moment(data.deliveryDate, "ddd, DD MMM YYYY").format("DD MMMM YYYY"),
        opts: {
          cellColWidth: 7000,
          align: "left",
          sz: "16",
        },
      },
    ],
    [
      {
        val: "Contact",
        opts: {
          cellColWidth: 7000,
          align: "left",
          b: true,
          sz: "16",
        },
      },
      {
        val: `+${data.contact.mobile.fullNumber}`,
        opts: {
          cellColWidth: 7000,
          align: "left",
          sz: "16",
        },
      },
      {
        val: "Buyer's Ref.:",
        opts: {
          cellColWidth: 7000,
          align: "left",
          b: true,
          sz: "16",
        },
      },
      {
        val: "",
        opts: {
          cellColWidth: 7000,
          align: "left",
          sz: "16",
        },
      },
    ],
  ];

  const detailsTable = getDetailsTable(deliveryOrder);

  const style = {
    "@w:val": "single",
    "@w:sz": "3",
    "@w:space": "1",
    "@w:color": "000000",
  };
  const borderStyle = {
    "w:top": style,
    "w:bottom": style,
    "w:left": style,
    "w:right": style,
  };

  const getCollectFromTable = (deliveryOrder) => {
    const darkOpts = {
      b: true,
      sz: "16",
      color: "ffffff",
      shd: {
        fill: "000000",
        themeFill: "text1",
        themeFillTint: "99",
      },
      fontFamily: "Calibri",
      cellColWidth: 20000,
      align: "left",
    };

    const table = [
      [
        {
          val: `Collect from ${deliveryOrder.collectFrom.length} Location(s)`,
          opts: { ...darkOpts },
        },
        {
          val: "Delivery to",
          opts: { ...darkOpts },
        },
      ],
    ];
    const opts = { sz: 16 };
    for (let i = 0; i < deliveryOrder.collectFrom.length; i++) {
      const current = deliveryOrder.collectFrom[i];
      const mid = i === Math.floor((deliveryOrder.collectFrom.length - 1) / 2);
      const merge = { vMerge: i === 0 || mid ? "restart" : "continue" };
      const locationLabel = [{ val: `Location ${i + 1}`, opts: { b: true, ...opts } }, { opts: { ...merge } }];
      const farm = [
        { val: `Fefifo Co-Farm @ ${current.farm.locationAlias.name}`, opts: { ...opts, b: true } },
        { val: mid ? deliveryOrder.orderedBy.name : "", opts: { ...merge, b: mid, ...opts, vAlign: "center" } },
      ];
      const address = [
        { val: current.pickupAddress, opts: { ...opts } },
        { val: mid ? deliveryOrder.delivery_address : "", opts: { ...merge, ...opts, vAlign: "center" } },
      ];
      const time = [{ val: "Scheduled time: ", opts: { b: true, ...opts } }, { opts: { ...merge } }];
      const divider = [{ val: "", opts: { b: true, ...opts } }, { opts: { ...merge } }];
      table.push(divider);
      table.push(locationLabel);
      table.push(farm);
      table.push(address);
      table.push(time);
      if (i === 0 || i === deliveryOrder.collectFrom.length - 1) table.push(divider);
    }

    const tableStyle = {
      align: "left",
      tableSize: 24,
      tableAlign: "left",
      borders: true,
      tableFontFamily: "Calibri",
    };
    const actionedByOpts = { sz: 16, b: true };
    const actionedByTable = [
      [
        { val: "Name: ", opts: { cellColWidth: 10000, ...actionedByOpts } },
        { val: deliveryOrder.actionedBy.name, opts: { cellColWidth: 10000, ...actionedByOpts } },

        { val: "Name: ", opts: { cellColWidth: 10000, ...actionedByOpts } },
        { val: "", opts: { cellColWidth: 10000, ...actionedByOpts } },
      ],
      [{ val: "Contact: ", opts: { ...actionedByOpts } }, "", { val: "Contact: ", opts: { ...actionedByOpts } }, ""],
    ];
    return [
      { type: "table", val: table, opt: { ...tableStyle, borderStyle: borderStyle } },
      { type: "table", val: actionedByTable, opt: { ...tableStyle } },
    ];
  };

  const collectFromTable = getCollectFromTable(deliveryOrder);

  const tableStyle = {
    // tableColWidth: 20000,
    align: "left",
    tableSize: 24,
    tableAlign: "left",
    borders: true,
    tableFontFamily: "Calibri",
  };

  const page2 = [];

  const opt = { sz: 16 };

  const getConfirmationTable = (collection = true) => [
    [
      {
        val: `Confirm & Sign When Items ${collection ? "Collected" : "Received"}`,
        opts: { ...darkOpts, cellColWidth: 10000, borderStyle: borderStyle },
      },
      {
        val: "Notes/Remarks",
        opts: { sz: 18, i: true, cellColWidth: 10000, vMerge: "restart" },
      },
    ],
    [
      {
        val: `${collection ? "Collection" : "Delivery"} Date: `,
        opts: { ...opt },
      },
      {
        opts: { vMerge: "continue" },
      },
    ],
    [
      {
        val: `${collection ? "Collection" : "Delivery"} Time: `,
        opts: { ...opt },
      },
      {
        opts: { vMerge: "continue" },
      },
    ],
    [
      {
        val: `All Items ${collection ? "Collected" : "Received"} in Good Order & Condition`,
        opts: { ...opt, i: true, vMerge: "restart" },
      },
      {
        opts: { vMerge: "continue" },
      },
    ],
    [
      {
        opts: { vMerge: "continue" },
      },
      {
        opts: { vMerge: "continue" },
      },
    ],
    [
      {
        opts: { vMerge: "continue" },
      },
      {
        opts: { vMerge: "continue" },
      },
    ],
    [
      {
        opts: { vMerge: "continue" },
      },
      {
        opts: { vMerge: "continue" },
      },
    ],
  ];

  const tableHeading = [
    {
      val: "S/N",
      opts: { ...darkOpts, cellColWidth: 2000 },
    },
    {
      val: "Description",
      opts: { ...darkOpts, cellColWidth: 20000 },
    },
    {
      val: "Yield\r\nUOM",
      opts: { ...darkOpts, cellColWidth: 4000, align: "center" },
    },
    {
      val: "Yield\r\nDelivered",
      opts: { ...darkOpts, cellColWidth: 4000, align: "center" },
    },
    {
      val: "Pacakging\r\nUOM",
      opts: { ...darkOpts, cellColWidth: 10000, align: "center" },
    },
    {
      val: "Qty\r\nDelivered",
      opts: { ...darkOpts, cellColWidth: 4000, align: "center" },
    },
  ];

  const divider = { type: "text", val: "" };

  for (let i = 0; i < deliveryOrder.collectionDetails.length; i++) {
    const current = deliveryOrder.collectionDetails[i];
    const farm = current.actual[0].farm;
    const label = {
      type: "text",
      val: `COLLECTION DETAILS: LOCATION ${i + 1}`,
      opt: { bold: true, font_size: 14 },
      lopt: { align: "center" },
    };
    const farmTable = [
      [
        {
          val: `Collect from Location 1: Fefifo Co-Farm @ ${farm.locationAlias.name}`,
          opts: { b: true, ...opt, cellColWidth: 20000, align: "left" },
        },
        {
          val: `Contact: ${deliveryOrder.actionedBy.name}`,
          opts: { b: true, ...opt, cellColWidth: 20000, align: "left" },
        },
      ],
      [
        {
          val: current.pickupAddress,
          opts: { ...opt },
        },
        {
          val: deliveryOrder?.actionedBy?.contact || "",
          opts: { ...opt },
        },
      ],
    ];

    const collectionTable = [tableHeading];
    for (let j = 0; j < current.actual.length; j++) {
      const cropModel = current.actual[j];
      const heading = [{ val: cropModel.cropModelCode, opts: { gridSpan: 6, sz: "16" } }];
      const cropLabel = cropModel.cropLabel || "[Crop Type] [Seed Variant]";
      const getPackaging = (grade) => grade?.shippingContainer || grade?.packaging;
      const getDeliveredAmt = (grade) => grade?.sizeOfContainer || grade?.capacityPerPack;
      let gradeA = tableRow(
        1,
        `${cropLabel} - Grade A`,
        cropModel.unitValue,
        cropModel.gradeAYield,
        getPackaging(cropModel.gradeA),
        getDeliveredAmt(cropModel.gradeA)
      );
      const nonGradeA = tableRow(
        2,
        `${cropLabel} - Non-Grade A`,
        cropModel.unitValue,
        cropModel.nonGradeAYield,
        getPackaging(cropModel.nonGradeA),
        getDeliveredAmt(cropModel.nonGradeA)
      );
      collectionTable.push(heading);
      collectionTable.push(gradeA);
      collectionTable.push(nonGradeA);
    }

    const confirmationTable = getConfirmationTable(true);

    const shipper = {
      type: "text",
      val: "Shipper Signature: \r\nShipper Name: \r\nContact: \r\n",
      opt: { font_size: 10 },
    };
    page2.push(label);
    page2.push({ type: "table", val: farmTable, opt: { ...tableStyle, borderStyle: borderStyle } });
    page2.push(divider);
    page2.push({ type: "table", val: collectionTable, opt: { ...tableStyle } });
    page2.push(divider);
    page2.push({ type: "table", val: confirmationTable, opt: { ...tableStyle } });
    page2.push(shipper);
  }

  const page3 = [
    {
      type: "text",
      val: `DELIVERY DETAILS`,
      opt: { bold: true, font_size: 14 },
      lopt: { align: "center" },
    },
  ];
  const deliveryTable = [tableHeading];

  for (let i = 0; i < deliveryOrder.deliveryDetails.length; i++) {
    const cropModel = deliveryOrder.deliveryDetails[i];
    console.log(i);
    const heading = [{ val: cropModel.cropModelCode, opts: { gridSpan: 6, sz: "16" } }];
    const cropLabel = cropModel.cropLabel || "[Crop Type] [Seed Variant]";
    const getPackaging = (grade) => grade?.shippingContainer || grade?.packaging;
    const getDeliveredAmt = (grade) => grade?.sizeOfContainer || grade?.capacityPerPack;
    let gradeA = tableRow(
      1,
      `${cropLabel} - Grade A`,
      cropModel.unitValue,
      cropModel.gradeAYield,
      getPackaging(cropModel.gradeA),
      getDeliveredAmt(cropModel.gradeA)
    );
    const nonGradeA = tableRow(
      2,
      `${cropLabel} - Non-Grade A`,
      cropModel.unitValue,
      cropModel.nonGradeAYield,
      getPackaging(cropModel.nonGradeA),
      getDeliveredAmt(cropModel.nonGradeA)
    );
    deliveryTable.push(heading);
    deliveryTable.push(gradeA);
    deliveryTable.push(nonGradeA);
  }

  const confirmationTable = getConfirmationTable(false);

  const recipient = {
    type: "text",
    val: "Recipient Signature: \r\nRecipient Name: \r\nContact: \r\n",
    opt: { font_size: 10 },
  };
  page3.push({ type: "table", val: deliveryTable, opt: { ...tableStyle } });
  page3.push(divider);
  page3.push({ type: "table", val: confirmationTable, opt: { ...tableStyle } });
  page3.push(recipient);

  console.log({ page3, deliveryTable });

  const data = [
    {
      type: "text",
      val: "Delivery Order",
      opt: { bold: true, font_size: 14 },
      lopt: { align: "center" },
    },
    { type: "table", val: detailsTable, opt: tableStyle },
    { type: "text", val: "" },
    ...collectFromTable,
    { type: "text", val: "" },
    { type: "pagebreak" },
    ...page2,
    { type: "pagebreak" },
    ...page3,
  ];
  return data;
};

export const getJsonForPaymentReceipt = (paymentReceipt) => {
  try {
    const getReceiptPaymentDetailsTable = (data) => [
      [
        {
          val: "Receipt No.:",
          opts: {
            cellColWidth: 5000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.receiptNo,
          opts: {
            cellColWidth: 10000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Date:",
          opts: {
            cellColWidth: 5000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.issueDate,
          opts: {
            cellColWidth: 10000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Received From:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.orderedBy.name,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Invoice No.:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.invoiceNo,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Name:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.contact.name,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
            b: true,
          },
        },
        {
          val: "Status:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: "Full Payment",
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Contact",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: `+${data.contact.mobile.fullNumber}`,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Payment Date:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: data.issueDate,
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
      [
        {
          val: "Payment Method:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: "",
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
        {
          val: "Payment Ref:",
          opts: {
            cellColWidth: 7000,
            align: "left",
            b: true,
            sz: "16",
          },
        },
        {
          val: "",
          opts: {
            cellColWidth: 7000,
            align: "left",
            sz: "16",
          },
        },
      ],
    ];

    const detailsTable = getReceiptPaymentDetailsTable(paymentReceipt);

    const tableStyle = {
      align: "left",
      tableSize: 24,
      tableAlign: "left",
      borders: true,
      tableFontFamily: "Calibri",
    };

    const darkOpts = {
      b: true,
      sz: "18",
      color: "ffffff",
      shd: {
        fill: "000000",
        themeFill: "text1",
        themeFillTint: "99",
      },
      fontFamily: "Arial",
    };

    const tableHeading = [
      {
        val: "S/N",
        opts: { ...darkOpts, cellColWidth: 2000 },
      },
      {
        val: "Description",
        opts: { ...darkOpts, cellColWidth: 20000 },
      },
      {
        val: "Qty",
        opts: { ...darkOpts, cellColWidth: 4000 },
      },
      {
        val: "UOM",
        opts: { ...darkOpts, cellColWidth: 4000 },
      },
      {
        val: "U/Price (MYR)",
        opts: { ...darkOpts, cellColWidth: 4000 },
      },
      {
        val: "Amount (MYR)",
        opts: { ...darkOpts, cellColWidth: 4000 },
      },
    ];

    let paymentReceiptTable = [tableHeading];

    for (let i = 0; i < paymentReceipt.deliveryDetails.length; i++) {
      const cropModel = paymentReceipt.deliveryDetails[i];
      const heading = [{ val: cropModel.cropModelCode, opts: { gridSpan: 6, sz: "16" } }];
      let gradeAContract = tableRow(
        1,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Grade A (Contracted)"}`,
        cropModel.postRejects.gradeAContractedQty,
        cropModel.unitValue,
        cropModel.gradeAPriceContract,
        cropModel.postRejects.gradeAContractedTotal !== 0
          ? `(${Math.abs(cropModel.postRejects.gradeAContractedTotalTotal)})`
          : 0
      );
      let gradeAUncontracted = tableRow(
        2,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Grade A (Uncontracted)"}`,
        cropModel.postRejects.gradeAUncontractedQty,
        cropModel.unitValue,
        cropModel.gradeAPriceSpot,
        cropModel.postRejects.gradeAUncontractedTotal !== 0
          ? `(${Math.abs(cropModel.postRejects.gradeAUncontractedTotal)})`
          : 0
      );
      let nonGradeA = tableRow(
        3,
        `${cropModel.cropLabel || "[Crop Type] [Seed Variant] - Non Grade A"}`,
        cropModel.postRejects.nonGradeAQty,
        cropModel.unitValue,
        cropModel.nonGradeAPrice,
        cropModel.postRejects.nonGradeATotal !== 0 ? `(${Math.abs(cropModel.postRejects.nonGradeATotal)})` : 0
      );
      let appComm = tableRow(
        4,
        `${cropModel.cropLabel || "Applicable Commission"}`,
        cropModel.postRejects.appCommQty,
        cropModel.unitValue,
        cropModel.appComm,
        cropModel.postRejects.appCommTotal
      );

      paymentReceiptTable.push(heading);
      paymentReceiptTable.push(gradeAContract);
      paymentReceiptTable.push(gradeAUncontracted);
      paymentReceiptTable.push(nonGradeA);
      paymentReceiptTable.push(appComm);
    }

    const total = paymentReceipt.totalDue < 0 ? `(${Math.abs(paymentReceipt.totalDue)})` : paymentReceipt.totalDue;
    const lastRow = (label, value) => [
      { val: label, opts: { sz: "16", b: true, align: "right", gridSpan: 5 } },
      {
        val: value,
        opts: { sz: "16", b: true, align: "center" },
      },
    ];
    paymentReceiptTable.push(lastRow("Total", total));
    paymentReceiptTable.push(lastRow("Amount Received", total));
    paymentReceiptTable.push(lastRow("Balance Due", "0.00"));

    const thankyou = getThankYouTable();
    const data = [
      {
        type: "text",
        val: "RECEIPT OF PAYMENT",
        opt: { bold: true, font_size: 14 },
        lopt: { align: "center" },
      },
      { type: "table", val: detailsTable, opt: tableStyle },
      { type: "text", val: "" },
      { type: "table", val: paymentReceiptTable, opt: tableStyle },
      { type: "text", val: "" },
      ...thankyou,
    ];
    return data;
  } catch (error) {
    console.log("error in getJsonForPaymentReceipt", error);
  }
};
